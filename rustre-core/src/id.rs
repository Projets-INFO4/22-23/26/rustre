use rustre_parser::ast::{AstToken, IdNode, IdRefNode, Ident};
use std::borrow::Cow;
use std::convert::Infallible;
use std::fmt::{Debug, Display, Formatter};
use std::str::FromStr;

/// Name of a node, param, variable, type, package, model, etc., as it appears in the declaration
///
/// This is simply a cheap newtype around `String`.
///
/// # Example values
///
///   * Nodes: `sin`, `adder`, `map`
///   * Variables: `a`, `b`, `cin`
///   * Types: `state`
///   * Packages: `Lustre`
///
/// # Usage
///
/// [Id]s do not remember their source span. Therefore, queries that rely on it may want to require
/// both to be given, or to compute the [Id] inside them. [Id]s without an accompanying span should
/// only be used as inputs to query that don't report the errors themselves.
#[derive(Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Id(str);

impl Id {
    pub const LUSTRE_PACKAGE: &'static Self = Id::from_str("Lustre");

    pub const fn from_str(str: &str) -> &Self {
        unsafe { std::mem::transmute(str) }
    }
}

impl Default for &'_ Id {
    fn default() -> Self {
        Id::from_str("")
    }
}

impl Default for Box<Id> {
    fn default() -> Self {
        <&Id>::default().to_owned()
    }
}

impl Clone for Box<Id> {
    fn clone(&self) -> Self {
        self.as_ref().to_owned()
    }
}

impl<'a> From<&'a str> for &'a Id {
    fn from(value: &'a str) -> Self {
        Id::from_str(value)
    }
}

impl From<Box<str>> for Box<Id> {
    fn from(value: Box<str>) -> Self {
        unsafe { std::mem::transmute(value) }
    }
}

impl From<String> for Box<Id> {
    fn from(value: String) -> Self {
        Self::from(value.into_boxed_str())
    }
}

impl<'a> From<&'a Id> for Cow<'a, Id> {
    fn from(value: &'a Id) -> Self {
        Self::Borrowed(value)
    }
}

impl From<Box<Id>> for Cow<'_, Id> {
    fn from(value: Box<Id>) -> Self {
        Self::Owned(value)
    }
}

impl ToOwned for Id {
    type Owned = Box<Id>;

    fn to_owned(&self) -> Self::Owned {
        let box_str = String::from(&self.0).into_boxed_str();
        Box::<Id>::from(box_str)
    }
}

impl Display for Id {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self.0)
    }
}

impl Debug for Id {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "`{}`", &self.0)
    }
}

/// Reference to a node, param, variable, type, package, model, etc., as it appears in the
/// usage / call site
///
/// This _may_ be identical to an [`Id`], but can also be preceded by a package name
/// (`package::member`).
///
/// # Example values
///
///   * Implicit packages (equivalent to simple [Id]s): `sin`, `a`, `state`
///   * Explicit package: `Lustre::add`, `Alu::shift_right`
///
/// # Usage
///
/// Like [`Id`]s, [IdRef]s don't store their source span. The [same tips][Id#usage] apply.
///
/// # Name resolution semantics
///
///   * If an [IdRef] has an explicit package name, the resolution is trivial
///   * If an [IdRef] has no explicit package name, IDs are first resolved in the current scope
///     (local or package), and then in the `Lustre::` package
#[derive(Clone, Hash, PartialEq)]
pub struct IdRef<'p, 'm> {
    package: Option<Cow<'p, Id>>,
    member: Cow<'m, Id>,
}

impl<'p, 'm> IdRef<'p, 'm> {
    pub fn new(package: Option<impl Into<Cow<'p, Id>>>, member: impl Into<Cow<'m, Id>>) -> Self {
        Self {
            package: package.map(|p| p.into()),
            member: member.into(),
        }
    }

    /// Creates an [IdRef] with an implicit (absent) package name
    pub fn new_implicit(member: impl Into<Cow<'m, Id>>) -> Self {
        Self {
            package: None,
            member: member.into(),
        }
    }

    /// Creates an [IdRef] with `Lustre::` for a package name
    pub fn new_lustre(member: impl Into<Cow<'m, Id>>) -> Self {
        Self {
            package: Some(Id::LUSTRE_PACKAGE.into()),
            member: member.into(),
        }
    }

    pub fn as_package(&self) -> Option<&Id> {
        self.package.as_deref()
    }

    pub fn as_member(&self) -> &Id {
        self.member.as_ref()
    }

    /// Retrieves the member, only if this IdRef has no explicit package
    ///
    /// Can be used to attempt local resolution of an identifier, which only makes sense when no
    /// package is specified.
    pub fn as_member_implicit(&self) -> Option<&Id> {
        Some(self.member.as_ref()).filter(|_| self.package.is_none())
    }

    pub fn member_eq(&self, other: &Id) -> bool {
        self.member.as_ref() == other
    }

    pub fn into_inner(self) -> (Option<Cow<'p, Id>>, Cow<'m, Id>) {
        (self.package, self.member)
    }
}

/// Converts the [`Id`] to an [`IdRef`] using [`IdRef::new_implicit`]
impl<'m> From<&'m Id> for IdRef<'_, 'm> {
    fn from(value: &'m Id) -> Self {
        Self::new_implicit(value)
    }
}

impl Display for IdRef<'_, '_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.package.as_deref() {
            Some(package) => write!(f, "{}::{}", &package.0, &self.member.0),
            None => write!(f, "{}", &self.member),
        }
    }
}

impl Debug for IdRef<'_, '_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.package.as_deref() {
            Some(package) => write!(f, "`{}::{}`", &package.0, &self.member.0),
            None => write!(f, "{:?}", &self.member),
        }
    }
}

/// Parses an IdRef from a &str
///
/// Most of the time, the actual Rustre parser will be used instead of this function. It exists
/// mostly for convenience and interoperability. Please note that this implementation is infallible,
/// and won't check for invalid chars in identifiers.
impl FromStr for IdRef<'_, '_> {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s.rsplit_once("::") {
            Some((package, model)) => Self::new(
                Some(Id::from_str(package).to_owned()),
                Id::from_str(model).to_owned(),
            ),
            None => Self::new_implicit(Id::from_str(s).to_owned()),
        })
    }
}

// AST glue

impl<'a> From<&'a Ident> for &'a Id {
    fn from(value: &'a Ident) -> Self {
        Id::from_str(value.text())
    }
}

impl<'a> From<&'a Ident> for Box<Id> {
    fn from(value: &'a Ident) -> Self {
        Id::from_str(value.text()).to_owned()
    }
}

impl From<Ident> for Box<Id> {
    fn from(value: Ident) -> Self {
        (&value).into()
    }
}

impl<'a> From<&'a IdNode> for Box<Id> {
    fn from(value: &'a IdNode) -> Self {
        let ident = value.ident().expect("unparseable");
        ident.into()
    }
}

impl From<IdNode> for Box<Id> {
    fn from(value: IdNode) -> Self {
        (&value).into()
    }
}

impl From<&IdRefNode> for IdRef<'_, '_> {
    fn from(value: &IdRefNode) -> Self {
        let package = value.package().map(<Box<Id>>::from);
        let member = <Box<Id>>::from(value.member());
        Self::new(package, member)
    }
}

impl From<IdRefNode> for IdRef<'_, '_> {
    fn from(value: IdRefNode) -> Self {
        (&value).into()
    }
}
