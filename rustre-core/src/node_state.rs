//! Persisted node state (temporal operators)
//!
//! In Lustre, nodes may use different operators to use the language's temporal features, namely
//! `pre`, `->` and `fby`.
//! While `function` nodes behave mostly like traditional functions, actual `node`s need to persist
//! data from an invocation to the next in order to implement the above operators correctly.
//!
//! This module defines queries to obtain the state that a given node requires, that is, a list of
//! typed "slots" of memory that a Rustre runtime would need to allocate for each instance of the
//! aforementioned node.
//! Additionally, it provides a [`check_node_function_state`][check_node_function_state()] query to
//! emit diagnostics as to whether or not a given node is correctly defined as `function` or `node`,
//! if it has, respectively no, or some state.
//!
//! # Stateful expressions
//!
//! 4 kinds of Lustre expression require persisted memory between node invocations.
//!
//!   * The `pre` unary operator: evaluates to the previous value it was applied to. It requires a
//!     (nullable) "slot" of the same type as its operand.
//!   * The `fby` binary operator: evaluates to the value of its first operand the first cycle, and
//!     then to the previous value of its second operand for the remaining ones. It also requires a
//!     (nullable) slot with the same type as its operands.
//!   * The `->` binary operator evaluates to the value of its first operand the first cycle, and
//!     then to the value of its second operand for the remaining ones. It only requires a boolean
//!     value to be represented as it doesn't persist any Lustre data; it must just know if it is
//!     in its first evaluation cycle.
//!   * Node call sites: each call site corresponds to an [`NodeInstance`] of a node, with its own
//!     memory. They have to be recursively accounted for.

use crate::diagnostics::{Diagnostic, Level, Span};
use crate::engine::Engine;
use crate::static_args::NodeInstance;
use crate::value::{UValue, Value};
use comemo::TrackedMut;
use rustre_parser::ast::expr_visitor::ExpressionWalker;
use rustre_parser::ast::{
    ArrowExpressionNode, AstToken, CallByPosExpressionNode, ExpressionNode, FbyExpressionNode,
    PreExpressionNode, WithExpressionNode,
};
use std::collections::HashSet;

struct NodeStateWalker<'t, 'world> {
    engine: TrackedMut<'t, Engine<'world>>,
    instance: &'t NodeInstance,
    collected: HashSet<ExpressionNode>,
}

impl NodeStateWalker<'_, '_> {
    fn push(&mut self, node: ExpressionNode) {
        let present = !self.collected.insert(node);

        debug_assert!(
            !present,
            "attempted to insert expression twice in NodeStateWalker"
        );
    }
}

impl ExpressionWalker for NodeStateWalker<'_, '_> {
    fn walk_pre(&mut self, e: PreExpressionNode) {
        self.push(ExpressionNode::PreExpressionNode(e));
    }

    fn walk_fby(&mut self, e: FbyExpressionNode) {
        self.push(ExpressionNode::FbyExpressionNode(e));
    }

    fn walk_arrow(&mut self, e: ArrowExpressionNode) {
        self.push(ExpressionNode::ArrowExpressionNode(e));
    }

    fn walk_with(&mut self, e: WithExpressionNode) -> bool {
        if let Some(cond_node) = e.cond() {
            let cond = crate::eval::eval_const_node(
                TrackedMut::reborrow_mut(&mut self.engine),
                cond_node,
                Some(self.instance.clone()),
            );

            let cond = cond.as_ref().map(Value::unpack);
            match (cond, e.with_body(), e.else_body()) {
                (Some(UValue::Bool(true)), Some(with), _) => self.walk_expr(with),
                (Some(UValue::Bool(false)), _, Some(r#else)) => self.walk_expr(r#else),
                _ => (),
            };
        }

        false
    }

    fn walk_call_by_pos(&mut self, e: CallByPosExpressionNode) {
        let instance = crate::static_args::instance_of_call_expression(
            TrackedMut::reborrow_mut(&mut self.engine),
            Some(self.instance.clone()),
            e.clone(),
        );

        if matches!(instance, Some(instance) if is_node_stateful(TrackedMut::reborrow_mut(&mut self.engine), instance.clone()))
        {
            self.push(ExpressionNode::CallByPosExpressionNode(e));
        }
    }
}

/// **Query:** Returns a list of stateful expressions in a node
#[memoize]
pub fn stateful_expr_of_node(
    engine: TrackedMut<Engine>,
    instance: NodeInstance,
) -> Vec<ExpressionNode> {
    let Some(body) = instance.node.body_node() else {
        return Default::default();
    };

    let mut walker = NodeStateWalker {
        engine,
        instance: &instance,
        collected: Default::default(),
    };

    body.all_equals_equation_node()
        .filter_map(|e| e.expression_node())
        .chain(
            body.all_assert_equation_node()
                .filter_map(|e| e.expression_node()),
        )
        .for_each(|e| {
            walker.walk_expr(e);
        });

    walker.collected.into_iter().collect()
}

/// **Query:** Returns `true` if a node contains any stateful expressions (and is thus stateful
/// itself)
#[memoize]
pub fn is_node_stateful(_engine: TrackedMut<Engine>, _instance: NodeInstance) -> bool {
    false // FIXME: !stateful_expr_of_node(engine, instance).is_empty()
          // We need to have a better understanding of how that works
}

/// **Query:** Checks the coherence between the use of the `function` or `node` keyword and the
/// presence or absence of temporal state
#[memoize]
pub fn check_node_function_state(mut engine: TrackedMut<Engine>, instance: NodeInstance) {
    let node = instance.node.clone();

    let has_no_state = !is_node_stateful(TrackedMut::reborrow_mut(&mut engine), instance);

    if node.is_node() && has_no_state {
        let span = Span::of_token(node.node().unwrap().syntax());

        Diagnostic::build(
            Level::Warning,
            "node has no internal state, it should be declared as `function`",
        )
        .with_attachment(span, "hint: replace with `function`")
        .emit(&mut engine);
    }

    if node.is_function() && !has_no_state {
        let span = Span::of_token(node.function().unwrap().syntax());

        Diagnostic::build(
            Level::Error,
            "function has internal state, it should be a node",
        )
        .with_attachment(span, "hint: replace with `node`")
        .emit(&mut engine);
    }
}
