mod error;
pub mod test;
mod world;

use crate::diagnostics::Diagnostic;
use ecow::EcoVec;
pub use error::*;
use std::hash::{Hash, Hasher};
use std::path::Path;
pub use world::*;

#[derive(Clone)]
pub struct Engine<'world> {
    stdlib: Source,
    diagnostics: EcoVec<Diagnostic>,
    world: &'world dyn World,
}

impl<'world> Engine<'world> {
    pub fn new(world: &'world dyn World) -> Self {
        Self {
            stdlib: Source::stdlib(),
            diagnostics: EcoVec::new(),
            world,
        }
    }
}

impl Hash for Engine<'_> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.diagnostics.hash(state);
    }
}

#[track]
#[allow(clippy::needless_lifetimes)] // comemo::track doesn't allow implicit lifetimes
impl<'world> Engine<'world> {
    pub fn emit(&mut self, diagnostic: Diagnostic) {
        self.diagnostics.push(diagnostic);
    }

    pub fn diagnostics(&self) -> EcoVec<Diagnostic> {
        self.diagnostics.clone()
    }

    /// Returns the Lustre standard library source file
    pub fn stdlib(&self) -> Source {
        self.stdlib.clone()
    }

    /// Forwards to [`World::input_files`]
    pub fn input_files(&self) -> EcoVec<Source> {
        self.world.input_files()
    }

    /// Forwards to [`World::imported_file`]
    pub fn imported_file(&self, relative_to: &Source, path: &Path) -> Result<Source, FileError> {
        if relative_to.file().is_stdlib() {
            // Cannot import a file from the standard library
            Err(FileError::IsSpecial)
        } else {
            self.world.imported_file(relative_to, path)
        }
    }
}
