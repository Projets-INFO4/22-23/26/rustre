use ecow::{eco_format, EcoString};
use std::io;
use std::str::Utf8Error;
use std::string::FromUtf8Error;

/// Error that may occur while trying to read a file
#[derive(Debug, Clone, Hash, thiserror::Error)]
pub enum FileError {
    #[error("file not found")]
    NotFound,

    #[error("wrong file encoding")]
    Encoding,

    #[error("access denied")]
    AccessDenied,

    #[error("file is a directory")]
    IsDirectory,

    #[error("file is special")]
    IsSpecial,

    #[error("{0}")]
    Other(EcoString),
}

impl From<io::Error> for FileError {
    fn from(err: io::Error) -> Self {
        match err.kind() {
            io::ErrorKind::NotFound => Self::NotFound,
            io::ErrorKind::PermissionDenied => Self::AccessDenied,
            // Extract io::ErrorKind::IsADirectory when stabilized
            other => {
                let err = eco_format!("{other}");
                if err.contains("is a directory") {
                    FileError::IsDirectory
                } else {
                    FileError::Other(err)
                }
            }
        }
    }
}

impl From<Utf8Error> for FileError {
    fn from(_value: Utf8Error) -> Self {
        Self::Encoding
    }
}

impl From<FromUtf8Error> for FileError {
    fn from(_value: FromUtf8Error) -> Self {
        Self::Encoding
    }
}
