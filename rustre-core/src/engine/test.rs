use super::{FileError, FileId, Source, World};
use ecow::{eco_vec, EcoVec};
use std::path::Path;

/// [World] that contains a single file, ideal for testing
///
/// This world doesn't support includes at all.
pub struct SingleFile {
    source: Source,
}

impl From<EcoVec<u8>> for SingleFile {
    fn from(value: EcoVec<u8>) -> Self {
        let file_id = FileId::new("main.lus");
        let source_raw = std::str::from_utf8(value.as_slice()).unwrap();
        let source = Source::new(file_id, source_raw.to_owned());

        Self { source }
    }
}

impl SingleFile {
    pub fn from_source(source: impl Into<String>) -> Self {
        Self::from(EcoVec::<u8>::from(source.into().into_bytes()))
    }
}

impl World for SingleFile {
    fn input_files(&self) -> EcoVec<Source> {
        eco_vec![self.source.clone()]
    }

    fn imported_file(&self, _relative_to: &Source, _path: &Path) -> Result<Source, FileError> {
        Err(FileError::NotFound)
    }
}
