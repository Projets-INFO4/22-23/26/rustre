//! Standard library builtins implementation

use crate::engine::Engine;
use crate::id::Id;
use crate::static_args::{NodeInstance, StaticArgs};
use crate::types::Type;
use crate::value::UValue;
use crate::{get_signature, get_typed_signature, Signature, TypedSignature};
use comemo::TrackedMut;
use rustre_parser::ast::{AstNode, NodeNode, Root};
use rustre_parser::SyntaxNode;

const ITERATOR_FILL: &Id = Id::from_str("fill");
const ITERATOR_RED: &Id = Id::from_str("red");
const ITERATOR_FILLRED: &Id = Id::from_str("fillred");
const ITERATOR_MAP: &Id = Id::from_str("map");
const ITERATOR_BOOLRED: &Id = Id::from_str("boolred");

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum IteratorKind {
    Red = 1,
    Fill,
    FillRed,
    Map,
    BoolRed,
}

impl IteratorKind {
    pub fn for_id(id: &Id) -> Option<Self> {
        if id == ITERATOR_RED {
            Some(IteratorKind::Red)
        } else if id == ITERATOR_FILL {
            Some(IteratorKind::Fill)
        } else if id == ITERATOR_FILLRED {
            Some(IteratorKind::FillRed)
        } else if id == ITERATOR_MAP {
            Some(IteratorKind::Map)
        } else if id == ITERATOR_BOOLRED {
            Some(IteratorKind::BoolRed)
        } else {
            None
        }
    }
}

/// Returns whether the given AST node is inside the standard library virtual file
///
/// Some items in the standard library are treated in a specific way by the compiler. This function
/// can be used to ensure that a given AST node is defined within the standard library.
pub fn is_in_stdlib(engine: TrackedMut<Engine>, node: SyntaxNode) -> bool {
    let root = Root::expect(node.ancestors().last().unwrap_or(node));
    let file = crate::enclosing_file_of_root(engine, root);
    file.is_stdlib()
}

#[memoize]
pub fn get_iterator_kind(engine: TrackedMut<Engine>, node_node: NodeNode) -> Option<IteratorKind> {
    let name = node_node.id_node().map(|id| id.ident().unwrap());
    let iterator_kind = name
        .as_ref()
        .map(<&Id>::from)
        .and_then(IteratorKind::for_id);

    if iterator_kind.is_some() && is_in_stdlib(engine, node_node.syntax().clone()) {
        iterator_kind
    } else {
        None
    }
}

/// Returns whether the given node corresponds to the definition of one of the 5 builtin iterators
pub fn is_iterator(engine: TrackedMut<Engine>, node_node: NodeNode) -> bool {
    get_iterator_kind(engine, node_node).is_some()
}

const ITERATOR_STATIC_PARAM_NODE: &Id = Id::from_str("N");
const ITERATOR_STATIC_PARAM_LENGTH: &Id = Id::from_str("n");

/// Retrieves the node ("N") parameter of generic iterators (`fill`, `red`, `fillred` and `map`)
#[memoize]
fn get_iterator_param_node(_engine: TrackedMut<Engine>, static_args: &StaticArgs) -> NodeInstance {
    // TODO(unwrap) + wrong parameter kind?
    static_args
        .resolve_node(ITERATOR_STATIC_PARAM_NODE)
        .unwrap()
        .clone()
}

/// Retrieves the length ("n") parameter of generic iterators (`fill`, `red`, `fillred` and `map`)
#[memoize]
fn get_iterator_param_len(_engine: TrackedMut<Engine>, static_args: &StaticArgs) -> Option<usize> {
    let param_len = static_args.resolve_const(ITERATOR_STATIC_PARAM_LENGTH)?;

    let UValue::Int(param_len) = param_len.unpack() else {
        todo!("iterator parameter is not an int")
    };

    if param_len < 0 {
        // Diagnostic::build(Level::Error, "negative iterator array length")
        //     .with_attachment(, "this expression evaluates to {}")
        //     .emit(&mut engine);
        // TODO: static args lose AST information
        todo!("interator param < 0")
    }

    // PANIC: cannot panic, bounds have been checked
    Some(usize::try_from(param_len).unwrap())
}

/// Returns the signature of an iterator
///
/// The given node is expected to satisfy [`is_iterator`] and NOT be [`IteratorKind::BoolRed`].
#[memoize]
pub fn get_iterator_signature(mut engine: TrackedMut<Engine>, callee: NodeInstance) -> Signature {
    let iterator_kind =
        get_iterator_kind(TrackedMut::reborrow_mut(&mut engine), callee.node.clone())
            .expect("not an iterator definition");

    if iterator_kind == IteratorKind::BoolRed {
        unreachable!("this function shouldn't be called with a boolred iterator")
    } else {
        let param_node =
            get_iterator_param_node(TrackedMut::reborrow_mut(&mut engine), &callee.static_args);

        get_signature(engine, param_node)
    }
}

/// Returns the typed signature from an iterator call site, given its provided static args
#[memoize]
pub fn get_iterator_typed_signature(
    mut engine: TrackedMut<Engine>,
    callee: NodeInstance,
) -> TypedSignature {
    let iterator_kind =
        get_iterator_kind(TrackedMut::reborrow_mut(&mut engine), callee.node.clone())
            .expect("not an iterator definition");

    let name = callee.node.id_node().unwrap().ident().unwrap();

    if iterator_kind == IteratorKind::BoolRed {
        todo!("boolred is not implemented")
    } else {
        let param_node =
            get_iterator_param_node(TrackedMut::reborrow_mut(&mut engine), &callee.static_args);
        let param_len =
            get_iterator_param_len(TrackedMut::reborrow_mut(&mut engine), &callee.static_args);

        let Some(param_len) = param_len else {
            // TODO: support missing signatures
            //       Right now, we return a dummy signature, which means that irrelevant "too many
            //       arguments" diagnostics might appear.
            return TypedSignature::from_name(Some(name));
        };

        let param_node_signature = get_typed_signature(engine, param_node);

        if iterator_kind == IteratorKind::Map {
            param_node_signature.map_all_params(|ty, _| Type::Array {
                elem: Box::new(ty),
                size: param_len,
            })
        } else {
            // fill, red, and fillred litterally have the exact same behavior, the only difference
            // is whether they allow their input/output parameters to contain a single element.
            param_node_signature.map_all_params(|ty, idx| {
                if idx == 0 {
                    // First param (accumulator) is kept as-is
                    ty
                } else {
                    Type::Array {
                        elem: Box::new(ty),
                        size: param_len,
                    }
                }
            })
        }
    }
}
