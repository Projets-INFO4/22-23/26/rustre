use codespan_reporting::diagnostic::{Diagnostic as Report, Label, LabelStyle, Severity};
use codespan_reporting::files::{Error, Files, SimpleFile};
use codespan_reporting::term::termcolor::{ColorChoice, StandardStream};
use codespan_reporting::term::{emit as emit_report, Config as ReportConfig};
use comemo::TrackedMut;
use rustre_core::diagnostics::Level;
use rustre_core::engine::{Engine, FileId, Source};
use std::cell::RefCell;
use std::collections::HashMap;
use std::ops::Range;

struct FileCache<'a, 'world> {
    inner: RefCell<FileCacheInner<'a, 'world>>,
}

struct FileCacheInner<'a, 'world> {
    engine: TrackedMut<'a, Engine<'world>>,
    cache: HashMap<FileId, SimpleFile<&'static str, Source>>,
}

impl<'a, 'world> FileCache<'a, 'world> {
    pub fn new(engine: TrackedMut<'a, Engine<'world>>) -> Self {
        FileCache {
            inner: RefCell::new(FileCacheInner {
                engine,
                cache: HashMap::new(),
            }),
        }
    }

    pub fn file(&self, id: <Self as Files<'a>>::FileId) -> SimpleFile<&'static str, Source> {
        let mut slot = self.inner.borrow_mut();
        let FileCacheInner {
            ref mut engine,
            cache,
        } = &mut *slot;

        cache
            .entry(id.clone())
            .or_insert_with(|| {
                let sources = rustre_core::all_sources(TrackedMut::reborrow_mut(engine));
                let source = sources
                    .iter()
                    .find(|s| &s.file() == id)
                    .expect("file not loaded in engine");

                SimpleFile::new("", Clone::clone(source))
            })
            .clone()
    }
}

impl<'a> Files<'a> for FileCache<'a, '_> {
    type FileId = &'a FileId;
    type Name = std::path::Display<'a>;
    type Source = Source;

    fn name(&'a self, id: Self::FileId) -> Result<Self::Name, Error> {
        Ok(id.path().display())
    }

    fn source(&'a self, id: Self::FileId) -> Result<Self::Source, Error> {
        Ok(self.file(id).source().clone())
    }

    fn line_index(&'a self, id: Self::FileId, byte_index: usize) -> Result<usize, Error> {
        Files::line_index(&self.file(id), (), byte_index)
    }

    fn line_range(&'a self, id: Self::FileId, line_index: usize) -> Result<Range<usize>, Error> {
        Files::line_range(&self.file(id), (), line_index)
    }
}

pub fn print_diagnostics(mut engine: TrackedMut<Engine>, deny_warnings: bool) -> Result<(), u8> {
    rustre_core::check(TrackedMut::reborrow_mut(&mut engine));

    let effects = engine.diagnostics();

    let mut errors = 0usize;
    let mut warnings = 0usize;
    for diagnostic in effects {
        let severity = match diagnostic.level {
            Level::Warning => {
                warnings += 1;
                Severity::Warning
            }
            Level::Error => {
                errors += 1;
                Severity::Error
            }
        };

        let files = FileCache::new(TrackedMut::reborrow_mut(&mut engine));

        let file_for_attachment = diagnostic
            .attachments
            .iter()
            .map(|(span, _)| span.root.clone())
            .collect::<Vec<FileId>>();

        let report = Report::new(severity)
            .with_message(diagnostic.message)
            .with_labels(
                diagnostic
                    .attachments
                    .iter()
                    .zip(&file_for_attachment)
                    .map(|((span, message), file)| {
                        Label::new(LabelStyle::Secondary, file, span.start..span.end)
                            .with_message(message)
                    })
                    .collect(),
            )
            .with_notes(diagnostic.notes.into_iter().map(Into::into).collect());

        let mut writer = StandardStream::stderr(ColorChoice::Always);
        emit_report(&mut writer, &ReportConfig::default(), &files, &report).unwrap();
    }

    if errors > 0 || (deny_warnings && warnings > 0) {
        Err(1)
    } else {
        Ok(())
    }
}
