use ecow::EcoVec;
use rustre_core::engine::{FileError, FileId, Source, World};
use std::cell::RefCell;
use std::collections::HashMap;
use std::path::Path;
use tracing::Level;

#[derive(Default)]
pub struct FsWorld {
    input: EcoVec<Source>,
    files: RefCell<HashMap<FileId, Result<Source, FileError>>>,
}

impl FsWorld {
    pub fn new(input: EcoVec<Source>) -> Self {
        // TODO cache input
        Self {
            input,
            files: RefCell::default(),
        }
    }

    pub fn from_paths<P: AsRef<Path>>(
        iter: impl IntoIterator<Item = P>,
    ) -> Result<Self, Vec<FileError>> {
        let iter = iter.into_iter();
        let (cap, _) = iter.size_hint();

        #[allow(clippy::manual_try_fold)] // False positive
        let files = iter.map(|path| Self::read_source_file(path)).fold(
            Ok(Vec::with_capacity(cap)),
            |acc: Result<_, Vec<FileError>>, res| match (acc, res) {
                (Ok(mut files), Ok(file)) => {
                    files.push(file);
                    Ok(files)
                }
                (Err(mut errors), Err(error)) => {
                    errors.push(error);
                    Err(errors)
                }
                (Ok(_), Err(error)) => Err(vec![error]),
                (Err(errors), Ok(_)) => Err(errors),
            },
        )?;

        Ok(Self::new(files.into()))
    }

    /// Reads a source file from disk, without caching it
    fn read_source_file(path: impl AsRef<Path>) -> Result<Source, FileError> {
        debug!("reading from disk");
        std::fs::read(path.as_ref())
            .map_err(FileError::from)
            .and_then(|bytes| String::from_utf8(bytes.as_slice().into()).map_err(FileError::from))
            .map(|text| Source::new(FileId::new(path), text))
            .inspect_err(|error| debug!(?error, "error reading file"))
    }
}

impl World for FsWorld {
    #[instrument(level = Level::TRACE, skip(self))]
    fn input_files(&self) -> EcoVec<Source> {
        self.input.clone()
    }

    #[instrument(level = Level::TRACE, skip(self))]
    fn imported_file(&self, relative_to: &Source, path: &Path) -> Result<Source, FileError> {
        let mut sources = self.files.borrow_mut();
        let target = relative_to.file().path().parent().unwrap().join(path);
        let id = FileId::new(&target);
        sources
            .entry(id)
            .or_insert_with(|| Self::read_source_file(target))
            .clone()
    }
}
