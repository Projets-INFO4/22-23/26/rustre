#[macro_use]
extern crate tracing;

mod diagnostics;
mod world;

use std::path::PathBuf;

use crate::diagnostics::print_diagnostics;
use crate::world::FsWorld;
use clap::{Parser, Subcommand};
use comemo::Track;
use rowan::NodeOrToken;
use rustre_core::engine::Engine;
use rustre_parser::{ast::AstNode, lexer::Token, SyntaxNode, SyntaxToken};
use tracing_subscriber::EnvFilter;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    // Dumps AST generated from file
    Ast {
        file: Option<String>,
    },

    /// Generate a Graphviz graph for a given node
    Dot {
        file: PathBuf,

        /// Node to display as a graph
        #[clap(long, short)]
        node: String,
    },

    /// Check that a Lustre program is correct
    Check {
        file: PathBuf,

        #[clap(long, short = 'W')]
        /// If set, rustre will return a non-zero status code when it encounters a warning
        deny_warnings: bool,
    },

    // Compiles file
    Build {
        file: Option<String>,
    },
}

fn main() -> Result<(), u8> {
    tracing_subscriber::FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let cli = Cli::parse();

    match &cli.command {
        Commands::Ast { file } => {
            match file {
                Some(filename) => {
                    // TODO: there is a bug in logos, the last token is not always
                    // properly parsed when there is a missing line break at the end
                    // of the file

                    let world = FsWorld::from_paths([filename]).unwrap();
                    let mut engine = Engine::new(&world);
                    let engine = engine.track_mut();
                    for root in rustre_core::all_roots(engine) {
                        print(0, NodeOrToken::Node(root.syntax().clone()));
                    }

                    Ok(())
                }
                None => {
                    println!("Missing argument : file");
                    Err(1)
                }
            }
        }
        Commands::Dot { file: _, node: _ } => {
            todo!()
        }
        Commands::Check {
            file,
            deny_warnings,
        } => {
            let world = FsWorld::from_paths([file]).unwrap();
            let mut engine = Engine::new(&world);
            let engine = engine.track_mut();
            print_diagnostics(engine, *deny_warnings)
        }
        Commands::Build { file } => match file {
            Some(_filename) => {
                println!("Ça construit un compilateur ou quoi ?? (PAS ENCORE PRÊT)");
                Ok(())
            }
            None => {
                println!("Missing argument : file");
                Err(1)
            }
        },
    }
}

fn print(indent: usize, element: rowan::NodeOrToken<SyntaxNode, SyntaxToken>) {
    let kind: Token = element.kind();
    print!("{:indent$}", "", indent = indent);
    match element {
        rowan::NodeOrToken::Node(node) => {
            println!("- {:?}", kind);
            for child in node.children_with_tokens() {
                print(indent + 2, child);
            }
        }

        NodeOrToken::Token(token) => println!("- {:?} {:?}", token.text(), kind),
    }
}
