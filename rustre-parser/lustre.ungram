// This file should not be read as a grammar, but rather as "what properties/children
// a given node can have?"

// === ProgramRules ===

// For instance, in the actual grammar, it is mandatory that either PackageBody or
// PackageList are present, but we want to consider them as optional properties
// to generate two getters that return an Option<_>
Root = IncludeStatement* ConstantDeclNode* TypeDeclNode* ExternalNodeDeclNode* NodeNode* ModelDeclNode* PackageDeclNode* PackageAliasNode*

IncludeStatement = 'str'

// === PackageRules ===

PackageDeclNode = 'package' IdNode UsesNode? ProvidesListNode? 'body'  'end' // TODO packbody
UsesNode = 'uses' IdNode*
PackageAliasNode = 'package' // TODO

// === ModelRules ===

ProvidesListNode = 'provides' ProvidesNode*
ProvidesNode = 'const'? // TODO
ModelDeclNode = 'model' IdNode UsesNode? 'needs' 'semicolon' ProvidesListNode? 'body'  'end' // TODO packbody

// === IdentVIRules ===

IdRefNode = 'ident'* 'colon'?

// === IdentRules ===

IdNode = 'ident' PragmaNode?
PragmaNode = 'percent'* 'ident'* 'colon'

// === NodesRules ===

TypedIdsNode = IdNode* 'comma'? 'colon' TypeNode
NodeNode = 'unsafe'? 'node'? 'function'? IdNode StaticParamsNode? NodeProfileNode 'equal'? alias:EffectiveNodeNode? VarDeclNode* OneConstantDeclNode* BodyNode // TODO
NodeProfileNode = 'returns' // Both the params Params and the return Params are `impl`emented in ast.rs
ParamsNode = VarDeclNode*
VarDeclNode = TypedIdsNode* ClockExpressionNode?

// === ConstantDeclRules ===

ConstantDeclNode = 'const' OneConstantDeclNode*
// ConstantDeclListNode = OneConstantDeclNode OneConstantDeclNode*
OneConstantDeclNode = IdNode* TypeNode? ExpressionNode?
// OneConstantDeclNode = IdNode (TypeNode | IdNode IdNode* TypeNode | TypeNode ExpressionNode | ExpressionNode)

// === TypeDeclRules ===

TypeDeclNode = 'type' OneTypeDeclNode*
OneTypeDeclNode = 'ident' TypeNode? EnumDeclNode? StructDeclNode?
EnumDeclNode = 'enum' IdNode*
StructDeclNode = 'struct' // TODO

// === SimpleTypeRules ===

TypeNode = 'bool'? 'int'? 'real'? IdRefNode? 'hat'? power:ExpressionNode?

// === ExtNodesRules ===

ExternalNodeDeclNode = 'unsafe'? 'extern'? 'node'? 'function'? IdNode NodeProfileNode

// === StaticRules ===

StaticParamsNode = StaticParamNode*
StaticParamNode = 'type'? 'const'? 'unsafe'? 'node'? 'function'? IdNode? TypeNode? NodeProfileNode?
StaticArgsNode = 'open_static_par'? StaticArgNode* 'close_static_par'?
StaticArgNode = TypeNode? ExpressionNode? EffectiveNodeNode? IdRefNode?
EffectiveNodeNode = IdRefNode StaticArgsNode

// === BodyRules ===

BodyNode = EqualsEquationNode* AssertEquationNode*
EqualsEquationNode = LeftNode 'equal' ExpressionNode
AssertEquationNode = 'assert' ExpressionNode

// === LeftRules ===

LeftNode = 'open_par'? LeftItemNode* 'comma'* 'close_par'?
LeftItemNode = IdNode | LeftFieldAccessNode | LeftTableAccessNode
LeftFieldAccessNode = LeftItemNode 'dot' IdNode
LeftTableAccessNode = LeftItemNode 'open_bracket' scalar_index:ExpressionNode select_index:SelectNode 'close_bracket'
SelectNode = left:ExpressionNode 'c_dots' right:ExpressionNode StepNode?
StepNode = 'step' ExpressionNode

// === ExpressionRules ===

ExpressionNode = ConstantNode
    | IdentExpressionNode
    | NotExpressionNode
    | NegExpressionNode
    | PreExpressionNode
    | CurrentExpressionNode
    | IntExpressionNode
    | RealExpressionNode
    | WhenExpressionNode
    | FbyExpressionNode
    | ArrowExpressionNode
    | AndExpressionNode
    | OrExpressionNode
    | XorExpressionNode
    | ImplExpressionNode
    | EqExpressionNode
    | NeqExpressionNode
    | LtExpressionNode
    | LteExpressionNode
    | GtExpressionNode
    | GteExpressionNode
    | DivExpressionNode
    | ModExpressionNode
    | SubExpressionNode
    | AddExpressionNode
    | MulExpressionNode
    | PowerExpressionNode
    | IfExpressionNode
    | WithExpressionNode
    | DieseExpressionNode
    | NorExpressionNode
    | ParExpressionNode
    | CallByPosExpressionNode
    | ArrayAccessExpressionNode
    | HatExpressionNode

IdentExpressionNode = IdRefNode
ParExpressionNode = ExpressionNode
ClockExpressionNode = 'not'? IdNode // TODO
ExpressionListNode = ExpressionNode*

NotExpressionNode = 'not' operand:ExpressionNode
NegExpressionNode = 'minus' operand:ExpressionNode
PreExpressionNode = 'pre' operand:ExpressionNode
CurrentExpressionNode = 'current' operand:ExpressionNode
IntExpressionNode = 'int' operand:ExpressionNode
RealExpressionNode = 'real' operand:ExpressionNode
WhenExpressionNode = left:ExpressionNode 'when' right:ExpressionNode
FbyExpressionNode = left:ExpressionNode 'f_by' right:ExpressionNode
ArrowExpressionNode = left:ExpressionNode 'arrow' right:ExpressionNode
AndExpressionNode = left:ExpressionNode 'and' right:ExpressionNode
OrExpressionNode = left:ExpressionNode 'or' right:ExpressionNode
XorExpressionNode = left:ExpressionNode 'xor' right:ExpressionNode
ImplExpressionNode = left:ExpressionNode 'impl' right:ExpressionNode
EqExpressionNode = left:ExpressionNode 'equal' right:ExpressionNode
NeqExpressionNode = left:ExpressionNode 'neq' right:ExpressionNode
LtExpressionNode = left:ExpressionNode 'lt' right:ExpressionNode
LteExpressionNode = left:ExpressionNode 'lte' right:ExpressionNode
GtExpressionNode = left:ExpressionNode 'gt' right:ExpressionNode
GteExpressionNode = left:ExpressionNode 'gte' right:ExpressionNode
DivExpressionNode = left:ExpressionNode 'div'? 'slash'? right:ExpressionNode
ModExpressionNode = left:ExpressionNode 'mod' right:ExpressionNode
SubExpressionNode = left:ExpressionNode 'minus' right:ExpressionNode
AddExpressionNode = left:ExpressionNode 'plus' right:ExpressionNode
MulExpressionNode = left:ExpressionNode 'star' right:ExpressionNode
PowerExpressionNode = left:ExpressionNode 'power' right:ExpressionNode
IfExpressionNode = 'if' cond:ExpressionNode 'then' if_body:ExpressionNode 'else' else_body:ExpressionNode
WithExpressionNode = 'with' cond:ExpressionNode 'then' with_body:ExpressionNode 'else' else_body:ExpressionNode
DieseExpressionNode = 'diese' list:ExpressionListNode
NorExpressionNode = 'nor' list:ExpressionListNode
CallByPosExpressionNode = node_ref:IdRefNode StaticArgsNode? 'open_par' args:ExpressionNode* 'close_par'
HatExpressionNode = left:ExpressionNode 'hat' right:ExpressionNode
ArrayAccessExpressionNode = left:ExpressionNode 'open_bracket' scalar_index:ExpressionNode select_index:SelectNode 'close_bracket'

// === ConstantRules ===

ConstantNode = 'true'? 'false'? 'i_const'? 'r_const'? // TODO Make enum
